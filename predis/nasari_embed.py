import sys
import re
import gensim
import numpy as np

nasariPath = '/Users/antoniomauro/Downloads/NASARI_embed_english.txt'

#REFACTOR WITH KEY ONLY BABEL SENSE ID
def NasariTemp():
    i = 0
    with open('tempNasari.txt','w') as out:
        for line in open(nasariPath, 'r'):
            if (i == 0):
                i += 1
                out.write(line)
                continue
            if (i > 3): break
            #print(line)
            i += 1
            #TEST
            words = re.split(r' +', line)
            print(len(words))
            synset = words[0][:12]
            out.write(synset)
            for x in range (1,301):
                out.write(' ' + words[x])
            #out.write('\n')

def embed(word):
    try:
        model = gensim.models.KeyedVectors.load_word2vec_format('tempNasari.txt')
        wordvec = model[word]
        #print(wordvec)
        #being of size 300, use np.resize to 50 due to performance
        wordvec = np.resize(wordvec,50)
        print(wordvec)
    except:
        wordvec = np.zeros(50)
    return wordvec

if __name__ == '__main__':
    embed('bn:00000002n')