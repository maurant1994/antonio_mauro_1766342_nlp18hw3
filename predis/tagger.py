import re
import numpy as np
from nltk.corpus import propbank
import random
from predis.selectional_preference import findArgSelctionalPref

def writeOutput(labels, verbs=None):
    with open('./1766342_test.txt', 'w') as out, open('./CoNLL2009-ST-English-development_label.txt', 'r') as input:
        #clear(input)
        #print(len(labels))
        lines = [line.rstrip('\n') for line in input]
        ind_lines = 0
        for label, verb in zip(labels, verbs):
            index = 1
            line = lines[ind_lines]
            verbCount = dict()
            for idx, v in enumerate(verb.values()):
                try:
                    validVerbs = len(propbank.roleset(v).findall("roles/role")) +1 #INCREASE F1 OF 1%
                except:
                    validVerbs = 4 #ARBITRARY SUPPOSE 4 ELEMENTS at most
                verbCount[idx] = [0,validVerbs] #FIRST ELEM IS STARTING POSITION EX. A0

            while True:
                words = re.split(r'\t+', line)
                if (words[0] is None or words[0] == ''):  # with space as words[0] starts a new sentence
                    out.write('\n')
                    #index += 1
                    ind_lines +=1
                    break

                out.write(line)
                if (index >= len(label)):
                    print('error')
                    break
                for num, k in enumerate(label[index]):
                    if (num >= len(verb)):
                        break

                    #if (verbCount[num] == -1):
                     #   continue
                    #random.randint(1,3)
                    if (k == 1.0) and (verbCount[num][1] > verbCount[num][0]):
                        #print('yes')
                        #MISSES NOT CLEAR RULE: AM-EXT - AM-PRD - AM-CAU
                        if (words[10] == 'ADV'):
                            if (words[4] == 'RB'):
                                out.write('\tAM-NEG')
                            else:
                                out.write('\tAM-ADV')
                        elif (words[10] == 'TMP'):
                            out.write('\tAM-TMP')
                        elif (words[10] == 'LOC'):
                            out.write('\tAM-LOC')
                        elif (words[10] == 'ROOT'):
                            out.write('\tAM-MOD')
                        elif (words[10] == 'DIR'):
                            out.write('\tAM-DIR')
                        elif (words[10] == 'DEP'):
                            out.write('\tAM-DIS')
                        else:
                            if (verbCount[num][0] <=1):
                                if (words[10] =='SBJ' or words[11] =='SBJ'):
                                    out.write('\tA0')
                                elif (words[10] =='OBJ' or words[11] == words[11] =='OBJ'):
                                    out.write('\tA1')
                                else:
                                    out.write('\tA' + str(verbCount[num][0]))
                            else:
                                out.write('\tA'+str(verbCount[num][0]))
                            verbCount[num][0] += 1
                        #HERE WRITE A1,A2,.. WITH TMP MODIFIER ... TODO
                    else:
                        # print('no')
                        out.write('\t_')

                out.write('\n')
                index += 1
                ind_lines += 1
                line = lines[ind_lines]

def writeOutputSelPref(labels, verbs=None):
    with open('./1766342_test.txt', 'w') as out, open('./CoNLL2009-ST-English-development_label.txt', 'r') as input, open('./sel_pref/dev.eval', 'r') as plain:
        lines = [line.rstrip('\n') for line in input]
        linesPlain = [line.rstrip('\n') for line in plain]
        ind_lines = 0
        sentenceCtr = 0
        for label, verb in zip(labels, verbs):
            index = 1
            line = lines[ind_lines]
            linePlain = linesPlain[ind_lines]
            verbCount = dict()
            for idx, v in enumerate(verb.values()):
                verbCount[idx] = [0,v] #FIRST ELEM IS STARTING POSITION EX. A0

            while True:
                #if (sentenceCtr >= 22): break DUE TO DISALIGNMENT, ELSE USE 2 LIST NOT ZIP OF FILE
                words = re.split(r'\t+', line)
                wordsSense = re.split(r'\t+', linePlain)
                if (words[0] is None or words[0] == ''):  # with space as words[0] starts a new sentence
                    out.write('\n')
                    #index += 1
                    ind_lines +=1
                    sentenceCtr += 1
                    break

                out.write(line)

                if (index >= len(label)):
                    print('error')
                    break

                for num, k in enumerate(label[index]):
                    if (num >= len(verb)):
                        break
                    sense = wordsSense[1]
                    if (k == 1.0) and sense != '_':
                        val = findArgSelctionalPref(v,sense)
                        out.write('\t' + val)
                    else:
                        out.write('\t_')

                out.write('\n')
                index += 1
                ind_lines += 1
                line = lines[ind_lines]
                linePlain = linesPlain[ind_lines]

def clear (out) :
    with open('./CoNLL2009-ST-English-development.txt', 'r') as input:
        lines = [line.rstrip('\n') for line in input]
        print(len(lines))
        for line in lines:
            words = re.split(r'\t+', line)
            if (words[0] is None or words[0] == ''):  # with space as words[0] starts a new sentence
                out.write('\n')
                continue
            for i in range(0, 13):
                out.write(words[i] + '\t')
            out.write(words[13] + '\n')

if __name__ == '__main__':
    gold = np.ones((150, 10))
    writeOutput(gold)