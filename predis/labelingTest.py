from nltk.corpus import propbank
import re

expose = propbank.roleset('get.01')
for role in expose.findall("roles/role"):
    print(role.attrib['n'], role.attrib['descr'])

correct = 0
lenx = 0
leny = 0
result = dict()
with open('./1766342_test.txt', 'r') as out, open('./CoNLL2009-ST-English-development.txt', 'r') as input:
    for liney, linex in zip(input, out):
        liney = liney.strip().lower()
        linex = linex.strip().lower()
        wordsy = re.split(r'\t+', liney)
        wordsx = re.split(r'\t+', linex)
        if (len(wordsx) - 14 < 0):
            continue
        #lenx += len(wordsx) - 14
        #leny += len(wordsy) - 14
        for i in range(14, len(wordsy)):
            try:
                if (wordsy[i] != '_'):
                    leny += 1
                if (wordsx[i] != '_'):
                    lenx += 1
                if (wordsy[i] != '_' and (wordsx[i] == wordsy[i] or wordsx[i] == wordsy[i][2:])): #if (wordsy[i] != '_' and wordsx[i] != '_'): #COUNR R-A0 AS A0, INCREASE 2%
                    correct += 1
            except:
                continue

    print('***', leny, lenx)
    P = correct / float(lenx)
    R = correct / float(leny)
    C = lenx / float(leny)
    if (P + R == 0):
        F1 = 0
    else:
        F1 = 2 * P * R / float((P + R))
    result['precision %'] = P * 100.0
    result['recall %'] = R * 100.0
    result['coverage %'] = C * 100.0
    result['f1 %'] = F1 * 100.0
    print(result)