import numpy as np
from predis.reader import generateGoldForTest
from predis.reader import generateVerbForBatch
from predis.tagger import writeOutput as writeOutput #writeOutputSelPref or writeOutput
import os

#"""
mergedLabels = np.load('label.npy') #label for dev or label_train
#np.set_printoptions(threshold=np.nan)
#print(mergedLabels)
print(len(mergedLabels))
#mergedLabels = []
#for i in range (0, 13334):
#    mergedLabels.append(np.ones((150,30)))
writeOutput(mergedLabels, generateVerbForBatch(150,path="./CoNLL2009-ST-English-development.txt"))
os.system("python3 labelingTest.py")
#"""

"""
batch_ys = generateGoldForTest(150,path="./CoNLL2009-ST-English-train.txt")
#np.set_printoptions(threshold=np.nan)
#print(batch_ys)
labels = []
for i in range(len(batch_ys)):
    batch_gold = batch_ys[i]
    labels.append(batch_gold)
    if (i % 1000 == 0):
        print('round ' + str(i))
        
print('now merge')
verbs = generateVerbForBatch(150,path="./CoNLL2009-ST-English-train.txt")
mergedLabels = []
lblidx = 0 # index for label
for obj in verbs:
    temp = np.zeros((150, 30)) #MAYBE TUNE VALUE
    for i in range(0, len(obj.values())):
        #print(i)
        if (i == 30):
            break
        temp[:,i] = labels[lblidx][:,0]
        lblidx += 1
    mergedLabels.append(temp)
#np.set_printoptions(threshold=np.nan)
#print(mergedLabels)
print('just save')
np.save('label_train.npy', mergedLabels)
writeOutput(mergedLabels, verbs)
"""