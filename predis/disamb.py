import sys
sys.path.append('../')
import re
import requests
import nltk
import ast
import sys, traceback
from collections import defaultdict
from nltk.corpus import wordnet as wn

#{'bn:00097147a': {'start': 1, 'end': 2}, 'bn:00112143a': {'start': 2, 'end': 2}, 'bn:00067181n': {'start': 3, 'end': 3}, 'bn:00007140n': {'start': 7, 'end': 7}, 'bn:00054416n': {'start': 8, 'end': 8}, 'bn:00077860n': {'start': 9, 'end': 9}, 'bn:00077868n': {'start': 9, 'end': 10}, 'bn:00025917n': {'start': 10, 'end': 10}, 'bn:00258768n': {'start': 13, 'end': 13}}
#key
#antonio : e4808e01-6c67-417b-a061-80a30514cd9a
#ola : f233c572-715b-4e0a-ba6d-5c329627e628
def get_babelfy(sent):
    r = requests.get(
        'https://babelfy.io/v1/disambiguate?lang=EN&key=e4808e01-6c67-417b-a061-80a30514cd9a&text=' + sent)
    #print(r.json())

    orig_stdout = sys.stdout
    f = open('./disamb.out', 'a+')
    sys.stdout = f

    print(r.json())

    sys.stdout = orig_stdout
    f.close()
    if not r.json():
        return 'Not json, ERROR!'
    #else:
     #   for element in r.json():
      #      wordSense[element['babelSynsetID']] = element['tokenFragment'] #score

        #sorted_data = sorted(wordSense.items(), key=operator.itemgetter(1), reverse=True)
        #concept = next(iter(sorted_data))[0]
        #print(concept)

        #d = requests.get('https://babelnet.io/v4/getSynset?id=' + concept + '&key=f233c572-715b-4e0a-ba6d-5c329627e628')
        #answer = d.json()['mainSense'][:-4]
    return 'hi'

def getSenseInvBabel():
    wordSenseInv = defaultdict(list)
    with open('./semcor_ims/test.txt', 'r') as input:
        sentences = [line.rstrip('\n') for line in input]
        sent = ""
        for k, line in enumerate(sentences):
            #get_babelfy(line)
            #print(i)
            #sent += line + " "
            if (k == 5): #fino 300
                break
        #print(sent)
    #print(wordSense)
    #print(tokenized)
    with open('./disamb.test.out', 'r') as test:
        lines = [line.rstrip('\n') for line in test]
        for idx, line in enumerate(lines):
            if (line == '{\'message\': \'Wrong parameters or language not supported.\'}'):
                continue
            tokenized = nltk.word_tokenize(sentences[idx])
            wordSenses = ast.literal_eval(line)
            wordSenseInvTemp = defaultdict(list)
            for j in range (0, len(wordSenses)):
                wordSense = wordSenses[j]
                value = wordSense['tokenFragment']
                temp = ''
                for i in range(value['start'], value['end'] + 1):
                    if (i != value['start']):
                        temp += ' '
                    if (i >= len(tokenized)):
                        break
                    temp += tokenized[i]
                    wordSenseInvTemp[wordSense['babelSynsetID']].append(temp)
            wordSenseInv[idx].append(wordSenseInvTemp)
    return wordSenseInv

def getSenseInvBabelNumber():
    wordSenseInv = defaultdict(list)
    with open('./disamb.test.out', 'r') as test:
        lines = [line.rstrip('\n') for line in test]
        for idx, line in enumerate(lines):
            if (line == '{\'message\': \'Wrong parameters or language not supported.\'}'):
                continue
            wordSenses = ast.literal_eval(line)
            wordSenseInvTemp = defaultdict(list)
            for j in range (0, len(wordSenses)):
                wordSense = wordSenses[j]
                value = wordSense['tokenFragment']
                for i in range(value['start'], value['end'] + 1):
                    wordSenseInvTemp[wordSense['babelSynsetID']].append(i)
            wordSenseInv[idx].append(wordSenseInvTemp)
    return wordSenseInv

def getCONLLPropbankList():
    #DICT PROPBANK - BABELNET
    wordSenseInv = defaultdict(list)
    counter = 0
    wordSenseInvTemp = dict()
    for line in open("./CoNLL2009-ST-English-test.txt", 'r'):
        #if (counter == 300): break
        line = line.strip().lower()
        words = re.split(r'\t+', line)
        if (words[0] == None or words[0] == ''):  # with space as words[0] starts a new sentence
            wordSenseInv[counter].append(wordSenseInvTemp)
            counter += 1
            wordSenseInvTemp = dict()
        elif (words[12] == 'y'):
            wordSenseInvTemp[words[0]] = words[13]
    return wordSenseInv

def executeMatchPB():
    propbank = getCONLLPropbankList()
    babel = getSenseInvBabelNumber()
    #print(propbank)
    #print(babel)
    matchList = defaultdict(list)
    for num, map in propbank.items():
        #print(babel[num])
        if (len(babel[num]) < 1): continue
        babelMap = babel[num][0]
        for key, values in babelMap.items():
            for k, v in map[0].items():
                for value in values:
                    if ((int)(k) == value):
                        matchList[v].append(key)
    print(len(matchList))
    with open("./propbank-babel.txt", 'w') as out:
        for key, values in matchList.items():
            out.write(key + ' - ')
            for value in values:
                out.write(value)
                out.write(' ')
            out.write('\n')
    return matchList

def executeMatchBP():
    matchList = executeMatchPB()
    map = defaultdict(list)
    with open("./babel-propbank.txt", 'w') as out:
        for key, values in matchList.items():
            for val in values:
                map[val].append(key)
        for key, values in map.items():
            out.write(key + ' - ')
            for value in values:
                out.write(value)
                out.write(' ')
            out.write('\n')
    return map

def wnToBab(wn):
    try :
        r = requests.get(
            'https://babelnet.io/v5/getSynsetIdsFromResourceID?id=wn:' + wn + '&wnVersion=WN_30&source=WN&key=e4808e01-6c67-417b-a061-80a30514cd9a')
        obj = r.json()
        print(obj)
        val = obj[0]['id']
        return val
    except :
        return 'UNK'

def executeMatchBabelWordNet():
    with open("./semcor_ims/index.sense", 'r') as input:
        sentences = [line.rstrip('\n') for line in input]
        print(len(sentences))

        orig_stdout = sys.stdout
        f = open('./wordnet-babel.txt', 'w')

        for line in sentences:
            try:
                line = line.strip().lower()
                w = re.split(r' +', line)
                keySyn = w[0]
                synset = wn.lemma_from_key(keySyn).synset()
                sense = synset.name()
                sense = sense[sense.index('.') + 1:]
                pos = sense[:sense.index('.')]
                off = str(synset.offset())  # some exception, but do not waste
                wnet = off.rjust(8, '0') + pos
                bn = wnToBab(wnet)
                print(wnet + '\t' + bn)
            except:
                print('\n')

        sys.stdout = orig_stdout
        f.close()

if __name__ == '__main__':
    #print(getSenseInvBabelNumber())
    #print(executeMatchPB())
    #print(executeMatchBP())
    print(wnToBab('08950407n'))
    executeMatchBabelWordNet()