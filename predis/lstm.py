from __future__ import print_function

import tensorflow as tf
from tensorflow.contrib import rnn
import os
from predis.tagger import writeOutput
import numpy as np
import re

# Import data
from predis.reader import generate_batch as generate_batch
from predis.reader import generateVerbForBatch as generateVerbForBatch

# Training Parameters
learning_rate = 0.001
training_steps = 9 #10000
batch_size = 150 # use as max word x sentence
display_step = 2000 #every tot disply status

# Network Parameters
timesteps = 52
num_hidden = 80
num_classes = 1

#Treshold
treshold = 0.5

# tf Graph input
X = tf.placeholder("float", [None, timesteps, 1])
Y = tf.placeholder("float", [None, num_classes])

# Define weights
weights = {
    # Hidden layer weights => 2*n_hidden because of forward + backward cells
    'out': tf.Variable(tf.random_normal([2*num_hidden, num_classes])) #GAUSSIAN RANDOM 0,1 -> ,mean=0.5, stddev=0.5
}
biases = {
    'out': tf.Variable(tf.random_normal([num_classes]))
}

# Saver
saver = tf.train.Saver()

def BiRNN(x, weights, biases):
    x = tf.unstack(x, timesteps, 1)

    # Define lstm cells with tensorflow
    # Forward direction cell
    lstm_fw_cell = rnn.BasicLSTMCell(num_hidden) #, forget_bias=1.0
    # Backward direction cell
    lstm_bw_cell = rnn.BasicLSTMCell(num_hidden) #, forget_bias=1.0

    # Get lstm cell output
    try:
        outputs, _, _ = rnn.static_bidirectional_rnn(lstm_fw_cell, lstm_bw_cell, x,
                                              dtype=tf.float32)
    except Exception: # Old TensorFlow version only returns outputs not states
        outputs = rnn.static_bidirectional_rnn(lstm_fw_cell, lstm_bw_cell, x,
                                        dtype=tf.float32)

    # Linear activation, using rnn inner loop last output
    return tf.matmul(outputs[-1], weights['out']) + biases['out']

logits = BiRNN(X, weights, biases)
prediction = tf.nn.sigmoid(logits) #softmax or sigmoid

# Define loss and optimizer
loss_op = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits( # softmax_cross_entropy_with_logits - sparse_softmax_cross_entropy_with_logits
    logits=prediction, labels=Y)) #FIXME
#NEW LOSS WITH SIGMOID AS PREDICTION
lossVal = -(Y * tf.log(prediction + 1e-12) + (1 - Y) * tf.log( 1 - prediction + 1e-12))
cross_entropy = tf.reduce_mean(tf.reduce_sum(lossVal, reduction_indices=[1]))

optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate) #GradientDescentOptimizer AdamOptimizer
#global_step = tf.Variable(0, trainable=False, name='global_step')
#increment_global_step = tf.assign_add(global_step,1, name = 'increment_global_step') #FOR CHECKPOINT PURPOSE
train_op = optimizer.minimize(cross_entropy)

# Evaluate model (with test logits, for dropout to be disabled)
#correct_pred = tf.equal(tf.argmax(prediction, 1), tf.argmax(Y, 1))
#tf.cast(tf.greater(prediction, 0.5 * tf.ones_like(prediction)), tf.float32) #HYPERDEFINE NUM 0,5 .. -1.0
correct_pred = tf.equal(tf.cast(tf.less(prediction, treshold * tf.ones_like(prediction)), tf.float32), tf.cast(Y, tf.float32)) #FIXME GREATER OR LESS ?, with lesst start all 1
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()

#batch_test = 0
#batch_gold = 0
# Start training
with tf.Session() as sess:

    # Run the initializer
    sess.run(init)
    #"""
    start = 1
    try:
        ckpt = tf.train.get_checkpoint_state('./model/')
        saver.restore(sess, ckpt.model_checkpoint_path)
        start = int(os.path.basename(ckpt.model_checkpoint_path).split('-')[1])
        print('checkpoint')
    except:
        print('No ckeckpoint')
    #"""
    print(start)
    if (start < training_steps):
        batch_xs, batch_ys, wordLens = generate_batch(batch_size)
    for step in range(start, training_steps):
        # Reshape data
        print('Start - Total round ', len(batch_xs))
        BATCH_UNIFIED = 5
        batch_x_temp = []
        batch_y_temp = []
        for i in range(len(batch_xs)):
            wordLen = wordLens[i]
            batch_x = batch_xs[i]
            batch_y = batch_ys[i]
            batch_x = batch_x.reshape((batch_size, timesteps, 1))
            #batch_y = batch_y.reshape((batch_size, num_classes))
            #batch_test = batch_x
            #batch_gold = batch_y

            #ADD ATENTION
            #multiply ADD 1, SQUUEZE, REMOVE 1, KEEP SAFE IN LEGTH ELEM
            with tf.name_scope('attention'):
                batch_x = batch_x[0:wordLen,0:timesteps,0:1] #100 IS 50 WORD EMBEDDING*POS + 50 VERB EMBEDDING
                batch_y = batch_y[0:wordLen, 0:1]
                #print('*******')
                #print(len(batch_y))
                #print(len(batch_y[0]))
                #print('*******')

            """
            if (i % BATCH_UNIFIED != 0 or i == 0):
                batch_x_temp += batch_x
                batch_y_temp += batch_y
                continue
            else :
                batch_x = batch_x_temp
                batch_y = batch_y_temp
                print (len(batch_x))
                batch_x_temp = []
                batch_y_temp = []
            """

            # Run optimization op (backprop)
            sess.run(train_op, feed_dict={X: batch_x, Y: batch_y})
            if (i % display_step == 0):
                # Calculate batch loss and accuracy
                #print(sess.run([prediction], feed_dict={X: batch_x}))
                loss, acc = sess.run([cross_entropy, accuracy], feed_dict={X: batch_x,
                                                                     Y: batch_y}) #cross_entropy or loss_op

                print("Step " + str(step) + "-Round Word " + str(i) + ", Minibatch Loss= " + \
                      "{:.4f}".format(loss) + ", Training Accuracy= " + \
                      "{:.3f}".format(acc*100.0) + "%, Progress in % ", (i/len(batch_xs))*100)
        saver.save(sess, './model/' + 'model.ckpt', global_step=step+1)
        print('Finished roudnd '  + str(step))

    print("Optimization Finished!")

    #####################
    # NOW TEST
    #######################
    # best = sess.run([prediction], {X: batch_x})
    # print(best)

    batch_xs, batch_ys, wordLens = generate_batch(batch_size,path="./CoNLL2009-ST-English-development.txt")
    result = dict()
    result['precision %'] = 0.0
    result['recall %'] = 0.0
    result['coverage %'] = 0.0
    result['f1 %'] = 0.0
    labels = []

    print('Start - Total round ', len(batch_xs))
    for i in range(len(batch_xs)):
        wordLen = wordLens[i]
        batch_test = batch_xs[i]
        batch_gold = batch_ys[i]
        batch_test = batch_test.reshape((batch_size, timesteps,1))
        #batch_gold = batch_gold.reshape((batch_size, num_classes))

        with tf.name_scope('attention'):
            batch_test = batch_test[0:wordLen, 0:100, 0:1]  # 100 IS 50 WORD EMBEDDING*POS + 50 VERB EMBEDDING
            batch_gold = batch_gold[0:wordLen, 0:1]
            # print('*******')
            # print(len(batch_y))
            # print(len(batch_y[0]))
            # print('*******')

        # Run optimization op (backprop)
        sess.run([prediction], feed_dict={X: batch_test})
        outPred = tf.cast(prediction, tf.float32)
        #print(outPred.eval(feed_dict={X: batch_test}, session=sess))

        outPred = tf.cast(tf.greater(outPred, treshold * tf.ones_like(outPred)), tf.float32) #HYPERDEFINE NUM TRESHOLD 0,5 .. -1.0
        toLabel = outPred.eval(feed_dict={X: batch_test}, session=sess)
        #print(toLabel)
        #THERE EVALUATE ACCURACY, RECALL F1, SAVED TESTED MODEL
        # Evaluation
        # Calculate Statistical information...
        correct = 0
        for k in range(0,len(toLabel)):
            for h, j in zip(toLabel[k], batch_gold[k]):
                if h == j and j != 0:
                    correct += 1

        #ADD LABEL FOR TAGGING
        labels.append(toLabel) #ADD GOLD FOR TEST NOW, TRUE APPEND TOLABEL -- batch_gold -- toLabel
        # knowing 10 feature classess (10 lines up to be labeled), for better analyses 4
        P = correct / float(len(toLabel))
        R = correct / float(len(batch_gold))
        C = len(toLabel) / float(len(batch_gold))
        if (P + R == 0):
            F1 = 0
        else:
            F1 = 2 * P * R / float((P + R))

        result['precision %'] += P
        result['recall %'] += R
        result['coverage %'] += C
        result['f1 %'] += F1
        if (i % display_step == 0):
            print("Round Test N.",i , ' Progress in % ', (i/len(batch_xs))*100)

    result['precision %'] /= len(batch_xs)
    result['recall %'] /= len(batch_xs)
    result['coverage %'] /= len(batch_xs)
    result['f1 %'] /= len(batch_xs)
    print(result)
    verbs = generateVerbForBatch(batch_size,path="./CoNLL2009-ST-English-development.txt")
    #print(verbs)
    #print(labels)
    #NOW MERGE LABELS
    mergedLabels = []
    lblidx = 0 # index for label
    for obj in verbs:
        temp = np.zeros((150, 30)) #MAYBE TUNE VALUE
        for i in range(0, len(obj.values())):
            #print(i)
            if (i == 10):
                break
            temp[:,i] = np.pad(labels[lblidx][:,0],(0, 150 - len(labels[lblidx])), 'constant') #PAD DUE TO USE CODE DEFINED AT THE BEGINNING OF DEVELOPPING, WORKS EVERY SIZE
            lblidx += 1
        mergedLabels.append(temp)
    #np.set_printoptions(threshold=np.nan)
    #print(mergedLabels)
    #np.save('label.npy', mergedLabels)
    writeOutput(mergedLabels, verbs)
    os.system("python3 labelingTest.py") #evaluate model

    #NOW CHECK PRECISION F1 RECALL....
    """
    correct = 0
    with open('./1766342_test_srl.txt', 'r') as out, open('./CoNLL2009-ST-English-development_label.txt', 'r') as input:
        for liney, linex in zip(input, out):
            liney = liney.strip().lower()
            linex = linex.strip().lower()
            wordsy = re.split(r'\t+', liney)
            wordsx = re.split(r'\t+', linex)
            for i in range(13, len(wordsy)):
                if (wordsx.get(i,'?') == wordsy.get(i)):
                    correct += 1
        P = correct / float(len(toLabel))
        R = correct / float(len(batch_gold))
        C = len(toLabel) / float(len(batch_gold))
        F1 = 2 * P * R / float((P + R))

        result['precision %'] = P
        result['recall %'] = R
        result['coverage %'] = C
        result['f1 %'] = F1
        print(result)
    """