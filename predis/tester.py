import tensorflow as tf
import os
import sys
from predis.lstm import prediction, batch_size, timesteps, num_input, saver, X
from predis.reader import generate_batch

if __name__ == '__main__':
    print('HI')

    with tf.Session as session:
        try:
            ckpt = tf.train.get_checkpoint_state('./model/')
            saver.restore(session, ckpt.model_checkpoint_path)
            start = int(os.path.basename(ckpt.model_checkpoint_path).split('-')[1])
            print('checkpoint')
        except:
            print('No ckeckpoint')
            sys.exit(0)
        batch_xs, batch_ys = generate_batch(batch_size)
        result = dict()
        result['precision %'] = 0.0
        result['recall %'] = 0.0
        result['coverage %'] = 0.0
        result['f1 %'] = 0.0
        for i in range(len(batch_xs)):
            batch_test = batch_xs[i]
            batch_gold = batch_ys[i]
            batch_test = batch_test.reshape((batch_size, timesteps, num_input))
            # Run optimization op (backprop)
            session.run([prediction], feed_dict={X: batch_test})

            outPred = tf.cast(prediction, tf.float32)
            outPred = tf.round(outPred * 2.5)  # HYPER DEFINE NUM
            toLabel = outPred.eval(feed_dict={X: batch_test}, session=session)
            # print(toLabel)
            # THERE EVALUATE ACCURACY, RECALL F1, SAVED TESTED MODEL
            # Evaluation
            # Calculate Statistical information...
            correct = 0
            for k in range(0, len(toLabel) - 6):
                for h, j in zip(toLabel[k], batch_gold[k]):
                    if h == j:
                        correct += 1

            # knowing 10 feature classess (10 lines up to be labeled), for better analyses 4
            P = correct / float(len(toLabel))
            R = correct / float(len(batch_gold))
            C = len(toLabel) / float(len(batch_gold))
            F1 = 2 * P * R / float((P + R))

            result['precision %'] += P * 4.0
            result['recall %'] += R * 4.0
            result['coverage %'] += C * 100.0
            result['f1 %'] += F1 * 4.0
            if (i % 5 == 0):
                print("Round Test N.", i)

        result['precision %'] /= len(batch_xs)
        result['recall %'] /= len(batch_xs)
        result['coverage %'] /= len(batch_xs)
        result['f1 %'] /= len(batch_xs)
        print(result)