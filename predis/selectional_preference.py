from nltk.corpus import wordnet as wn
import re
from collections import defaultdict
from predis.semcor_formatter import formatCONLL

def most_common(lst):
    #MOST COMMON OR FIRST
    return max(set(lst), key=lst.count)

def postProcessIMS():
    with open('./sel_pref/dev.eval', 'w') as out, open('./sel_pref/dev.out', 'r') as input, open('./CoNLL2009-ST-English-development.txt', 'r') as conll:
        lines = [line.rstrip('\n') for line in input]
        linesDev = [line.rstrip('\n') for line in conll]
        i = -1  # ELSE AD INCREMENT AT END OF LOOP
        predProp = buildPropBankDictFromConll()
        #print(predProp)
        for index, line in enumerate(lines):
            words = re.split(r' +', line)
            for w in words:
                if (w[:8] == 'length="' or w == '<x'):
                    continue
                try:
                    keySyn = w[:w.index('|1.0"')]
                    word = w[w.index('|1.0">')+6:w.index('</')]
                    #print(word)
                    off = wn.lemma_from_key(keySyn).synset() #some exception, but do not waste
                    off = off.name() #off.offset().rjust(8,'0')
                    #print(off)
                    out.write(word + '\t' + off) #(vocab[w[:w.index('|1.0"')]])
                except:
                    out.write(w + '\t_')
                i += 1
                #CHECK IDENTIFIED ARG TODO
                curr = re.split(r'\t+', linesDev[i])
                for idx in range (14, len(curr)):
                    #print(predProp[index],idx)
                    verb = 'UNK'
                    if (len(predProp[index]) > idx - 14): #SOMETIMES TWO VERBS, DISALINEMENT ERROR
                        verb = predProp[index][idx -14]
                    out.write('\t' + curr[idx] + '\t' + verb ) # +
                out.write('\n')
            out.write('\n')

def buildPropBankDictFromConll():
    verbs = defaultdict(list)
    index = 0
    for line in open('./CoNLL2009-ST-English-development.txt', 'r'):
        words = re.split(r'\t+', line)
        if (words[0] != None and words[0] != '\n'):
            if (words[12] == 'Y'):
                verbs[index].append(words[13])
        else:
            index += 1
    #print(verbs) #OK
    return verbs

def buildVocabList():
    preds = defaultdict(lambda: defaultdict(list))
    with open('./sel_pref/dev.eval', 'r') as input:
        lines = [line.rstrip('\n') for line in input]
        for line in lines:
            words = re.split(r'\t+', line)
            #WORD SENSE ARG PRED ARG PRED ...
            #print(words)
            if (words[0] == None or words[0] == ''):
                #new line so add ?
                continue
            else :
                offset = words[1]
                #print(len(words))
                for index in range (2,len(words)-1):
                    if (index % 2 != 0): # step of 2
                        continue
                    arg = words[index]
                    if (arg == '_'):
                        continue
                    pred = words[index + 1]
                    #print(arg)
                    if (offset != '_'):
                        preds[pred][arg].append(offset)
    #print(preds) #OK
    return preds


def selectionalPreference(p,r,w):
    res = 1
    #print(p)
    #print(r)
    #print(w)
    for head in r:
        s1 = wn.synset(head) #word
        s2 = wn.synset(w) #word
        score = s1.path_similarity(s2)
        if (score is None):
            score = 0
        #print(score)
        score *= weight(head,r)
        res += score
    return score

def weight(w,r):
    counter = 0
    for head in r:
        if (head == w):
            counter += 1

    return counter / len(r)


def selectionalPreferenceOutput(word):
    max = 0
    val = 'UNK'
    pred = buildVocabList()
    #print(pred)
    for p in pred.keys():
        for arg, r in pred[p].items():
            #print(r)
            temp = selectionalPreference(p,r,word)
            #print(temp)
            if (temp > max):
                max = temp
                val = str(r) + '\t' + str(arg) + '\t' + str(p) + '\t' + str(max*100) + '%' #ex bat.n.01 A3 incentive.01 score
                #print(val)
    return val

def selPrefToLexCategory():
    pred = buildVocabList()
    lex = defaultdict(lambda: defaultdict(list))
    # print(pred)
    for p in pred.keys():
        for arg, r in pred[p].items():
            for syn in r:
                syn = wn.synset(syn)
                name = syn.lexname()
                #print(name)
                lex[str(p)][str(arg)].append(name)
    print(lex)
    return lex

def dictLexOne():
    lex = selPrefToLexCategory()
    # print(pred)
    for p in lex.keys():
        for arg, r in lex[p].items():
            lex[str(p)][str(arg)] = most_common(r)

    print(lex)
    return lex

def findArgSelctionalPref(p,word):
    max = 0
    val = 'UNK'
    pred = buildVocabList()
    # print(pred)
    for arg, r in pred[p].items():
        # print(r)
        temp = selectionalPreference(p, r, word)
        # print(temp)
        if (temp > max):
            max = temp
            val = str(arg) + '\t' + str(max*100) + '%'  # ex bat.n.01 A3 incentive.01 score
            val = str(arg)
    return val

def writeToFile():
    lex = dictLexOne()
    with open('./sel_pref/res.txt', 'w') as out:
        for p in lex.keys():
            for arg, r in lex[p].items():
                val = str(p) + ' - ' + str(arg) + ' : ' + str(r) + '\n'
                out.write(val)
        print(lex)
        return lex


if __name__ == '__main__':
    #formatCONLL(input="./CoNLL2009-ST-English-development.txt", output='./sel_pref/dev.txt')
    #postProcessIMS()
    #CMD ON IMS, PRODUCED 'dev.eval'
    #buildVocabList()
    #out = selectionalPreferenceOutput('dog.n.01')
    #out = findArgSelctionalPref('slice.01', 'get.v.01')
    #print(out)
    #selPrefToLexCategory()
    #dictLexOne()
    #writeToFile()
    synset = wn.synset('dog.n.01')
    print(synset.name())
    print(synset.lexname())