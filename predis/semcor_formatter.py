import re
import xml.etree.ElementTree
from nltk.corpus import wordnet as wn
from predis.nltk_model import extractPred
from predis.disamb import wnToBab
from predis.disamb import executeMatchBP

def formatCONLL(input="./CoNLL2009-ST-English-test.txt", output='./semcor_ims/testUpper.txt'):
    sentences = []
    sent = ""
    for line in open(input, 'r'):
        line = line.strip()
        words = re.split(r'\t+', line)
        if (words[0] == None or words[0] == ''):  # with space as words[0] starts a new sentence
            sentences.append(sent)
            sent = ""
        else:
            sent += words[1] + " "

    print(len(sentences))
    with open(output, 'w') as out:
        for sent in sentences:
            out.write(sent+'\n')

def preprocessSemCorKey():
    with open('./semcor_ims/semcor.gold.key', 'w') as out, open('./semcor/semcor.gold.key', 'r') as input:
        lines = [line.rstrip('\n') for line in input]
        for line in lines:
            out.write('BNC ' + line + '\n')

def wordWithHead(word):
    return '<head>' + word + '</head> '

def preprocessSemCor():
    with open('./semcor_ims/semcor.data.xml', 'w') as out, open('./semcor/semcor.data.xml', 'r') as input:
        out.write('<?xml version="1.0" encoding="UTF-8"?>\n' +
                  '<!DOCTYPE corpus SYSTEM "lexical-sample.dtd">\n' +
                  '<corpus lang="english">\n')

        root = xml.etree.ElementTree.parse(input).getroot()
        for i in range (0, len(root)): #iterate over TEXT  in root
            text = root[i]
            for j in range(0, len(text)): #iterate over SENTENCE in text
                sentence = text[j]
                lexelt = sentence.attrib['id']
                print(lexelt)
                print(len(sentence))
                #START WRITING
                out.write('<lexelt item="' + lexelt + '" pos="unk">\n')

                #initialize dictionary
                ctx = dict()
                for x in range(0, len(sentence)):
                    e = sentence[x]
                    if (e.tag == 'instance'):
                        ctx[e.attrib['id']] = ''
                #now fill
                for k in range (0, len(sentence)):
                    e = sentence[k]
                    for key in ctx.keys():
                        if (e.tag == 'instance' and key == e.attrib['id']):
                            ctx[key] += wordWithHead(e.text)
                        else :
                            ctx[key] += e.text + ' '
                #print(context)
                for instance, context in ctx.items():
                #if (len(instances) > 1) : #AVOID EMPTY DATAS
                    out.write('<instance id="' + instance + '" docsrc="BNC">\n')
                    out.write('<context>\n')
                    out.write(context + '\n')
                    out.write('</context>\n')
                    out.write('</instance>\n')

                #CLOSE IN LOOP
                out.write('</lexelt>\n')

        #END
        out.write('</corpus>')

def buildIndexSenseVocab():
    vocab = dict()
    with open('./semcor_ims/index.sense', 'r') as input:
        lines = [line.rstrip('\n') for line in input]
        for line in lines:
            words = re.split(r' +', line)
            vocab[words[0]] = words[1]
    return vocab

#use upper case due to some bugs
def evaluateOutIMS():
    #vocab = buildIndexSenseVocab()
    with open('./semcor_ims/test.eval', 'w') as out, open('./semcor_ims/testUpper.out', 'r') as input, open('./semcor_ims/testUpper.txt', 'r') as test:
        linesTest = [line.rstrip('\n') for line in test]
        lines = [line.rstrip('\n') for line in input]
        for line, lineTest in zip(lines, linesTest):
            words = re.split(r' +', line)
            preds = []
            for pred in extractPred(lineTest):
                preds.append(pred.index)
            i = -1 #ELSE AD INCREMENT AT END OF LOOP
            for w in words:
                if (w[:8] == 'length="' or w == '<x'):
                    continue
                try:
                    keySyn = w[:w.index('|1.0"')]
                    off = str(wn.lemma_from_key(keySyn).synset().offset()) #some exception, but do not waste
                    off = off.rjust(8,'0')
                    out.write(off) #(vocab[w[:w.index('|1.0"')]])
                except:
                    out.write(w)
                i += 1
                #CHECK IDENTIFIED VERBS
                #print(i)
                if (i in preds):
                    out.write('\ty')
                else:
                    out.write('\t_')
                out.write('\n')
            out.write('\n')

#DIS-ALIGNMENT ,
def scoreIMS():
    correct = 0 # 1 AVOID NULL POINTER
    lenx = 0
    leny = 0
    result = dict()
    with open('./semcor_ims/test.eval', 'r') as f1, open('./CoNLL2009-ST-English-test.txt', 'r') as f2:
        lines1 = [line.rstrip('\n') for line in f1]
        lines2 = [line.rstrip('\n') for line in f2]
        print(len(lines1),len(lines2))
        for liney, linex in zip(lines2, lines1):

            liney = liney.strip().lower()
            linex = linex.strip().lower()
            wordsy = re.split(r'\t+', liney)
            wordsx = re.split(r'\t+', linex)
            if(wordsx[0] == None or wordsx[0] == '' or wordsy[0] == None or wordsy[0] == ''):
                continue # with space as words[0] starts a new sentence
            if (wordsx[1] == 'y'):
                lenx += 1
            elif (wordsy[12] == 'y' and wordsy[4][:2] == 'vb'): #RESTRICTION
                leny += 1
            if (wordsx[1] == 'y' and wordsy[12] == 'y' and wordsy[4][:2] == 'vb'): #FAKE SYSTEM
                correct += 1

        print('***', leny, lenx)
        P = correct / float(lenx)
        R = correct / float(leny)
        C = lenx / float(leny)
        if (P + R == 0):
            F1 = 0
        else:
            F1 = 2 * P * R / float((P + R))
        result['precision %'] = P * 100.0
        result['recall %'] = R * 100.0
        result['coverage %'] = C * 100.0
        result['f1 %'] = F1 * 100.0
        print(result)

#ALIGNMENT PROBLEM SOLVED, BUT WASTE TIME
def evalScorIMSDic():
    with open('./semcor_ims/test.eval', 'r') as f1, open('./CoNLL2009-ST-English-test.txt', 'r') as f2:
        lines1 = [line.rstrip('\n') for line in f1]
        lines2 = [line.rstrip('\n') for line in f2]
        listx = []
        listy = []
        temp = ''
        for liney in lines2:
            liney = liney.strip().lower()
            wordsy = re.split(r'\t+', liney)
            if (wordsy[0] == None or wordsy[0] == ''):
                # with space as words[0] starts a new sentence
                listy.append(temp)
                temp = ''
                continue
            if (wordsy[12] == 'y' and wordsy[4][:2] == 'vb'):
                temp += wordsy[13] + '\t'
            else:
                temp += '_\t'
        temp = ''
        for linex in lines1:
            linex = linex.strip().lower()
            wordsx = re.split(r'\t+', linex)
            if (wordsx[0] == None or wordsx[0] == ''):
                listx.append(temp)
                temp = ''
                continue  # with space as words[0] starts a new sentence
            if (len(wordsx) > 2):
                temp += wordsx[2] + '\t'
            else:
                temp += wordsx[1] + '\t'

        print(len(listx), len(listy)) #OK

        #NOW EVAL
        correct = 0  # 1 AVOID NULL POINTER
        lenx = 0
        leny = 0
        result = dict()
        map = executeMatchBP()
        index = 1
        for xs,ys in zip (listx,listy):
            if (index == 5) : break
            x = re.split(r'\t+', xs)
            y = re.split(r'\t+', ys)
            lenT = len(x)
            if (len(y) < len(x)):
                lenT = len(y)
            for i in range(lenT):
                if (x[i] != '_'):
                    lenx += 1
                if (y[i] != '_'):
                    leny += 1
                if (y[i] != '_' and x[i] != '_'):
                    print (map[x[i]])
                    print(y[i])
                    if (len(map[x[i]]) > 0 and y[i] == map[x[i]][0]): #PROBLEM DUE TO NON COMPLETE ALIGNMENT use bottom
                    #if (y[i] in map[x[i]] or len(map[x[i]]) == 0):
                        correct += 1
            index += 1

        print('***', leny, lenx)
        P = correct / float(lenx)
        R = correct / float(leny)
        C = lenx / float(leny)
        if (P + R == 0):
            F1 = 0
        else:
            F1 = 2 * P * R / float((P + R))
        result['precision %'] = P * 100.0
        result['recall %'] = R * 100.0
        result['coverage %'] = C * 100.0
        result['f1 %'] = F1 * 100.0
        print(result)

def postProcessBabelEval():
    with open('./semcor_ims/test.eval', 'r') as input, open('./semcor_ims/testBab.eval', 'w') as out:
        lines = [line.rstrip('\n') for line in input]
        i = 1
        for line in lines:
            words = re.split(r'\t+', line)
            out.write(line)
            if (words[0] == None or words[0] == ''):
                out.write('\n')
                continue
            if (words[1] == 'y' and i <= 300): #END POS
                i += 1
                #val = words[0] + 'v'
                if (i <= 200): #START POS , waste 1 call, does not matter
                    print('done ', i)
                else :
                    val = wnToBab(words[0] + 'v')
                    print(val,i)
                    out.write('\t' + val)
            out.write('\n')

if __name__ == '__main__':
    #preprocessSemCorKey()
    #preprocessSemCor()
    #formatCONLL()
    #evaluateOutIMS()
    #scoreIMS()
    #postProcessBabelEval()

    evalScorIMSDic()
