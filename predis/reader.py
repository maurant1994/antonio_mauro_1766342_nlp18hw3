import re
import numpy as np
from gensim.models import KeyedVectors
from gensim.models import Word2Vec
from gensim.scripts.glove2word2vec import glove2word2vec
from predis.nasari_embed import embed as nasari_embed
from predis.disamb import executeMatchPB
class Features:
    pass

def buildVocabVerb(file_path="./CoNLL2009-ST-English-train.txt"):
    verbs = dict()
    index = 0
    for line in open(file_path, 'r'):
        line = line.strip().lower()
        words = re.split(r'\t+', line)
        if (words[0] != None and words[0] != ''):
            if (words[12] == 'y'):
                if words[13] not in verbs.values():
                    verbs[index] = words[13]
                    index += 1
    return verbs

def readFile(num, file_path="./CoNLL2009-ST-English-dev.txt"):
    sentences = []
    sentence = dict()
    verbsWithLabel = dict()
    verbsLabelVec = dict()
    gold = np.zeros((num, 30))
    pos = dict()
    #output = []
    i = 1;
    ctrLen = 1
    # glove_file = 'glove.6B/glove.6B.50d.txt'
    tmp_file = 'test_word2vec.txt'
    # glove2word2vec(glove_file, tmp_file)
    model = KeyedVectors.load_word2vec_format(tmp_file)
    modelPos = Word2Vec.load('pos2vec')
    modelVerb = Word2Vec.load('verb2vec')
    for line in open(file_path, 'r'):
        line = line.strip().lower()
        words = re.split(r'\t+', line)
        if (words[0] != None and words[0] != ''):  # with space as words[0] starts a new sentence
            ctrLen += 1
            vector = word_to_vector(words[2],model)
            sentence[i] = vector #lemma instead word[1]
            vector = pos2vec(words[4].upper(), modelPos)
            #if words[4].upper() not in output:
            #    output.append(words[4].upper())
            pos[i] = vector
            if (i < num) :
                for idx in range (14,44):
                    gold[i, idx - 14] = 0 if (len(words) < (idx + 1) or words[idx] == '_') else 1
                #gold[i, 0] = 0 if (len(words) < 15 or words[14] == '_') else 1
                #gold[i, 1] = 0 if (len(words) < 16 or words[15] == '_') else 1
                #gold[i, 2] = 0 if (len(words) < 17 or words[16] == '_') else 1
                #gold[i, 3] = 0 if (len(words) < 18 or words[17] == '_') else 1
            if (words[12] == 'y'):
                vector = verb_to_vector(words[13],modelVerb)
                verbsLabelVec[i] = vector #lemma instead word[1]
                vector = verb_to_vector(words[13],model)
                verbsWithLabel[i] = words[13] #vector
        else:
            obj = Features()
            obj.sentence = sentence
            obj.verbsWithLabel = verbsWithLabel
            obj.verbsLabelVec = verbsLabelVec
            obj.pos = pos;
            obj.gold = gold
            obj.ctrLen = ctrLen
            ctrLen = 1
            sentences.append(obj)
            sentence = dict()
            verbsWithLabel = dict()
            verbsLabelVec = dict()
            gold = np.zeros((num, 30))
            pos = dict()
            i = 0 #reset
        i +=1
    #print(output)
    return sentences

def pos2vec(pos, model):
    #TAG_MAP = [['IN', 'DT', 'NNP', 'CD', 'NN', '``', "''", 'POS', '(', 'VBN', 'NNS', 'VBP', ',', 'CC', ')', 'VBD', 'RB', 'TO', '.', 'VBZ', 'HYPH', 'PRP', 'PRP$', 'VB', 'JJ', 'MD', 'VBG', 'NNPS', 'WDT', 'RBR', ':', 'WP', 'PDT', 'RBS', 'JJR', 'JJS', 'WRB', '$', 'RP', 'EX', 'PRF', '#', 'UH', 'WP$', 'SYM', 'NIL', 'LS', 'FW']]
    #model = Word2Vec(TAG_MAP, size=len(TAG_MAP), window=5, min_count=1, workers=4)
    #model.save('pos2vec')
    try:
        return model.wv[pos]
    except KeyError:
        return np.zeros(1)

# Convert all words to vectors. Check for unk words,and append all vectors into a list.
# Return vector and updated word.
def word_to_vector(word, model, verb=False):
    try:
        wordvec = model[word]
        vector = wordvec  # list of  vector representation

    except KeyError:
        word = 'UNK'
        if verb:
            vector = np.zeros(150)
        else:
            vector = np.zeros(50)

    return vector

# Convert all words to vectors. Check for unk words,and append all vectors into a list.
# Return vector and updated word.
def verb_to_vector(word, model):
    # model = Word2Vec([verbs.values()], size=50, window=5, min_count=1, workers=4)
    # model.save('verb2vec')

    """
    #NASARI APPROACH PRESENTED, NOT REALIZED DUE TO LACK OF A BABELNET WSD
    map = executeMatchPB()
    if (word[:2] != 'bn'):
        word = map[word][0] #KEY PROPBANK - VALUE BABELNET
        return nasari_embed(word)
    """

    try:
        wordvec = model[word]
        vector = wordvec  # list of  vector representation

    except KeyError:
        word = 'UNK'
        vector = np.zeros(50)

    return vector

stopNum = 100000

def generate_batch(batch_size, num=50, path="./CoNLL2009-ST-English-train.txt"):

    print('go')
    x = np.empty((num+2, batch_size))
    xs = []
    ys = []
    lens = []
    #print(len(readFile2(batch_size, path))) = 39279

    j=0
    for obj in readFile(batch_size, path):
        idxvrb = 0
        for key, verb in obj.verbsLabelVec.items():
            for i in range(batch_size):
                """ #OPTIONS 1 flag woth sense embed NASARI OR AD-HOC
                if (i == key):
                    #print(verb)
                    x[:, i] = np.concatenate((obj.sentence.get(i, np.zeros(50)) * obj.pos.get(i, 0), verb), axis=0)
                else :
                    x[:, i] = np.concatenate((obj.sentence.get(i, np.zeros(50)) * obj.pos.get(i, 0), np.zeros(50)), axis=0)
                """
                #""" #OPTIONS 2 flag 0 - 1
                if (i == key):
                    #print(verb)
                    x[:, i] = np.concatenate((obj.sentence.get(i, np.zeros(50)) , obj.pos.get(i, np.zeros(1)), np.ones(1)), axis=0)
                else :
                    x[:, i] = np.concatenate((obj.sentence.get(i, np.zeros(50)) , obj.pos.get(i, np.zeros(1)), np.zeros(1)), axis=0)
                #"""
            #x[:, i] = obj.sentence.get(i, np.zeros(50)) * obj.pos.get(i, 0)
            #x[:, i, 1] = obj.pos.get(i, 0) #POS
            y = np.array([obj.gold[:,idxvrb]])
            #print(y)
            xs.append(x)
            #print(x)
            ys.append(y.T)
            x = np.empty((num+2, batch_size))
            idxvrb+=1
            lens.append(obj.ctrLen) # here because a sentence with N predicates is splitted in N sentences
        #y = np.empty((batch_size, 10))  # 10 output classes
        j+=1
        if (j > stopNum):
            break
    #np.set_printoptions(threshold=np.nan)
    #print(ys)
    print(lens)
    return xs, ys, lens

def generateVerbForBatch(batch_size, num=50, path="./CoNLL2009-ST-English-train.txt"):
    verbs = []
    j=0
    for obj in readFile(batch_size, path):
        y = obj.verbsWithLabel
        verbs.append(y)
        j+=1
        if (j > stopNum):
            break
    return verbs

def generateGoldForTest(batch_size, num=50, path="./CoNLL2009-ST-English-train.txt"):
    ys = []
    j = 0
    for obj in readFile(batch_size, path):
        idxvrb = 0
        for key, verb in obj.verbsLabelVec.items():
            y = np.array([obj.gold[:, idxvrb]])
            ys.append(y.T)
            idxvrb += 1
        j += 1
        if (j > stopNum):
            break
    return ys

if __name__ == '__main__':
    #just for funct as build vocab
    #verbs = buildVocabVerb()
    #print(verbs.values())
    #model = Word2Vec([verbs.values()], size=50, window=5, min_count=1, workers=4)
    #model.save('verb2vec')
    modelVerb = Word2Vec.load('verb2vec')
    print(verb_to_vector('take.01', modelVerb))

    generate_batch(150,path="./CoNLL2009-ST-English-development.txt")
